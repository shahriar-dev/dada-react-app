import React from "react"
import "./userlist.css"

import Table from "@mui/material/Table"
import TableBody from "@mui/material/TableBody"
import TableCell from "@mui/material/TableCell"
import TableContainer from "@mui/material/TableContainer"
import TableHead from "@mui/material/TableHead"
import TableRow from "@mui/material/TableRow"
import ArrowCircleRightIcon from "@mui/icons-material/ArrowCircleRight"
import AddCircleIcon from "@mui/icons-material/AddCircle"
import { Link } from "react-router-dom"

function createData(name, email) {
  return { name }
}

const rows = [
  createData("JOHN DOE"),
  createData("JOHN DOE"),
  createData("JOHN DOE"),
  createData("JOHN DOE"),
  createData("JOHN DOE"),
]

const Userlist = () => {
  return (
    <div className="userlist">
      <div className="border-bottom">
        <TableContainer>
          <Table aria-label="simple table">
            <TableHead>
              <TableRow>
                <TableCell component="th" scope="row">
                  Name
                </TableCell>
                <TableCell component="th" scope="row">
                  Action
                </TableCell>
                <TableCell>
                  <Link to="/add" className="link">
                    <AddCircleIcon />
                  </Link>
                </TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {rows.map((row) => (
                <TableRow
                  key={row.name}
                  sx={{ "&:last-child td, &:last-child th": { border: 0 } }}
                >
                  <TableCell scope="row">{row.name}</TableCell>
                  <TableCell align="left">
                    <Link to="/info" name={row.name}>
                      <ArrowCircleRightIcon />
                    </Link>
                  </TableCell>
                  <TableCell></TableCell>
                </TableRow>
              ))}
            </TableBody>
          </Table>
        </TableContainer>
      </div>
    </div>
  )
}

export default Userlist
