import axios from "axios"
import React, { useState } from "react"
import "./form.css"

const Form = ({ inputs }) => {
  const [values, setValues] = useState({})
  const [response, setResponse] = useState({})
  const api = axios.create({
    baseURL: "", // NEED TO PROVIDE SERVICE URL HERE
  })

  const onChange = (e) => {
    setValues({
      ...values,
      [e.target.name]: e.target.value,
    })
  }

  const register = (e) => {
    e.preventDefault()
    console.log(values)

    api
      .post(values)
      .then((response) => {
        console.log(response)
        setResponse(response)
      })
      .catch((error) => {
        console.log(error)
      })

    console.log(response)
  }

  return (
    <div className="form">
      <div className="top">
        <h1>Add New User</h1>
      </div>

      <div className="bottom">
        <div className="right">
          <form className="form-register" onSubmit={register}>
            {inputs.map((input) => (
              <div className="formInput" key={input._id}>
                <label>{input.label}</label>
                <input
                  type={input.type}
                  name={input.name}
                  placeholder={input.placeholder}
                  onChange={onChange}
                />
              </div>
            ))}
            <div className="formInput">
              <button type="submit">Submit</button>
            </div>
          </form>
        </div>
      </div>
    </div>
  )
}

export default Form
