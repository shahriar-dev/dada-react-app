import axios from "axios"
import React, { useState } from "react"
import "./container.css"

const Container = () => {
  const [values, setValues] = useState({})
  const [response, setResponse] = useState({})
  const api = axios.create({
    baseURL: "", // NEED TO PROVIDE SERVICE URL HERE
  })

  const onChange = (e) => {
    setValues({
      ...values,
      [e.target.name]: e.target.value,
    })
  }

  const postChat = (e) => {
    e.preventDefault()
    console.log(values)

    api
      .post(values)
      .then((response) => {
        console.log(response)
        setResponse(response)
      })
      .catch((error) => {
        console.log(error)
      })

    console.log(response)
  }

  return (
    <div className="container">
      <div className="top">
        <h1>Information Page</h1>
      </div>

      <div className="bottom">
        <div className="right">
          <form className="form-chat" onSubmit={postChat}>
            <div className="formInput">
              <input
                type="text"
                name="message"
                placeholder="Chat Here..."
                onChange={onChange}
              />
            </div>
            <div className="formInput">
              <button type="submit">Send</button>
            </div>
          </form>
        </div>
      </div>
    </div>
  )
}

export default Container
