import { BrowserRouter, Route, Routes } from "react-router-dom"
import { userInputs } from "./assets/formSource"
import AddUser from "./pages/addUser/AddUser"
import Home from "./pages/home/Home"
import Info from "./pages/info/Info"

function App({ props }) {
  return (
    <div className="App">
      <BrowserRouter>
        <Routes>
          <Route index element={<Home />} />
          <Route path="/add" element={<AddUser inputs={userInputs} />} />
          <Route path="/info" element={<Info />} />
        </Routes>
      </BrowserRouter>
    </div>
  )
}

export default App
