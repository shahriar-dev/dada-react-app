import React from "react"
import Form from "../../components/form/Form"
import Userlist from "../../components/userList/Userlist"
import "./addUser.css"

const AddUser = ({ inputs }) => {
  return (
    <div className="add-user">
      <Userlist />
      <Form inputs={inputs} />
    </div>
  )
}

export default AddUser
