import React from "react"
import Userlist from "../../components/userList/Userlist"
import Container from "../../components/container/Container"
import "./info.css"

const Info = () => {
  return (
    <div className="info">
      <Userlist />
      <Container />
    </div>
  )
}

export default Info
