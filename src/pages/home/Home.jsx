import React from "react"
import Userlist from "../../components/userList/Userlist"
import "./home.css"

const Home = () => {
  return (
    <div className="home">
      <Userlist />
    </div>
  )
}

export default Home
