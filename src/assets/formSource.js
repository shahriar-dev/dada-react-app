export const userInputs = [
  {
    id: 1,
    label: "Username",
    name: "username",
    type: "text",
    placeholder: "john_doe",
  },
  {
    id: 2,
    label: "Name and surname",
    name: "fullname",
    type: "text",
    placeholder: "John Doe",
  },
  {
    id: 3,
    label: "Email",
    name: "email",
    type: "mail",
    placeholder: "john_doe@gmail.com",
  },
]
